const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 8010;

app.use(bodyParser.urlencoded({ extended: false }));

// Homepage
app.get('/', (req, res) => {
    res.send('Welcome to our demo web application!');
});

// Form submission
app.post('/submit', (req, res) => {
    const { name, email } = req.body;
    res.send(`Form submitted: Name - ${name}, Email - ${email}`);
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
